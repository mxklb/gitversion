#!/bin/bash
# This script prints a sematic version number in the form: "X.Y.Z"
# The version number is fetched from the latest git tag!
# If not direct on tag appends "-#" = commits till tag.
# Note: This script needs/depends on git.

# Get version from latest git tag (uses '1.2.3' tags)
gitversion=$(git describe --long)
IFS='-' read -a versions <<< "$gitversion"
IFS='.' read -a semver <<< "${versions[0]}"

# Define patch ..
patch=${semver[2]}
if [ ${#versions[@]} -eq '3' ]; then
  if [ ${versions[1]} -ne '0' ]; then
    # Append commits away from tag ..
    patch="${semver[2]}-${versions[1]}"
  fi
fi

# Set version if untagged ..
if [ -z "${semver[0]}" ]; then
  version="0.0.0"
else
  version=$(echo "${semver[0]}.${semver[1]}.$patch")
fi

echo "$version"
