# Git Version

Simple project for semantic versioning of software projects using git.

## How to use it

Include this repository as a git submodule into the repository to be versioned:

    git submodule add git@gitlab.com:mxklb/gitversion.git

Now use sematic version tags `Major.Minor.Patch` within your parent repo:

    git tag -a 1.0.0 -m "Initial Version 1.0.0"

Now get version information from your parent repository by calling:

    gitversion/version.sh

This prints a semantic version `X.Y.Z` when the repository is checked out on a tagged commit.

**Note:** If not on a tagged commit a `-c` [commit counter till the latest tag] gets appended.
