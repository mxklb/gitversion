import os
import re

# Version printed if $VERSION is undefined
default = "wip"

# Check for default version file --> /tmp/version
defaultFile = '/tmp/version'
if os.path.isfile(defaultFile):
    with open(defaultFile) as f:
        default = f.read()

# Match semantic versioning string: https://gist.github.com/jhorsman/62eeea161a13b80e39f5249281e17c39
regex = re.compile('^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(-[a-zA-Z\d][-a-zA-Z.\d]*)?(\+[a-zA-Z\d][-a-zA-Z.\d]*)?$')

# Try to get semantic version string from environment variable $VERSION ..
def getVersion(undefined=None, append=True):
    '''
    1. If $VERSION is not defined -> default version string = "wip" is returned
    2. If $VERSION doesn't match semver regex -> returns `default-$VERSION` (if `append=False` only `default`)
    3. To use another differnet default/fallback version, set 'undefined' to any desired string
    '''
    defaultVersion = default
    if undefined != None:
        defaultVersion = undefined
    try:
        version = os.environ['VERSION']
        if regex.match(version) == None:
            append_env = ''
            if len(version) > 0 and len(defaultVersion) > 0:
                append_env = "-" + version
            version = defaultVersion
            if append == True:
                version = defaultVersion + append_env
        return version
    except:
        return defaultVersion

#print(getVersion())
#print(getVersion(undefined="horst"))
#print(getVersion(append=False))
